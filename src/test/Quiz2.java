package test;

public class Quiz2 {
	public static int size = 500;
	public static int min = 1;
	public static int max = 10;

	public static void main(String args[]) {
		//generate random array
		int[] num_array = Quiz2.random_array();
		
		//sorts it
		num_array=Quiz2.BubbleSort(num_array);
		
		//prints the array
		Quiz2.printNumberArray(num_array);
	}

	public static int[] random_array() {
		int[] number = new int[size];

		for (int i = 0; i < Quiz2.size; i++) {
			number[i] = Quiz2.random_number();
		}
		return number;
	}

	public static int random_number() {

		return (int) Math.round(Math.random() * (Quiz2.max - Quiz2.min)
				+ Quiz2.min);
	}

	static int[] BubbleSort(int[] array)
	{
	    int n = array.length;
	    for (int pass = 1; pass <= n - 1; pass++)
	        for (int i = 0; i < n - 1; i++)
	            if (array[i] > array[i + 1])
	            {
	                int temp = array[i];
	                array[i] = array[i + 1];
	                array[i + 1] = temp;
	            }
	    return array;
	}

	public static void printNumberArray(int[] numbers) {
		for (int i = 0; i < numbers.length; i++) {
			System.out.print(numbers[i] + " ");
		}
		System.out.println();

	}

}
